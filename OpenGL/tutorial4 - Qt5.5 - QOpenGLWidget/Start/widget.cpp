#include "widget.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <qopenglfunctions.h>

#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QOpenGLVertexArrayObject>

#include <QMouseEvent>
#include <QTimer>
#include <QPainter>

#include <iostream>
#include <cmath>


/* ----------------------------------------
 *
 * This is the window for the overall GL
 * application.
 *
 * ----------------------------------------
 */


// Constructor
GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
}

// Destructor
GLWidget::~GLWidget()
{
}

void GLWidget::initializeGL()
{
    // Initialize the color to black
    glClearColor(1.0f, 0.5f, 1.0f, 0.0f);
}
