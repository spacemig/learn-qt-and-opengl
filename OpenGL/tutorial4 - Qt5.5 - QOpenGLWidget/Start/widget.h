#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>

class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
     // The constructor
     explicit GLWidget(QWidget *parent = 0);

     // The destructor
     ~GLWidget();
    void initializeGL();

private:

};

#endif // WIDGET_H
