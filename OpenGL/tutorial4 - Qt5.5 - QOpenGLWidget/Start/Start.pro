#-------------------------------------------------
# Project Description:
# This is the start of my first actual OpenGL
# project.
#
# Name    : Christianne Izumigawa
# Created : 02-05-2016
# Modified: 02-05-2016
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Start
TEMPLATE = app

LIBS += -lopengl32 -lglu32 -lglut32 -glfw32 -glfw

SOURCES += main.cpp\
        widget.cpp \
    basic.cpp \
    foundation.cpp

HEADERS  += widget.h \
    basic.h \
    foundation.h



RESOURCES +=

DISTFILES += \
    shaders/color_shader.frag \
    shaders/vertex_shader.vsh\\\
