#ifndef OBJECTFILE_H
#define OBJECTFILE_H

#include <QFile>
#include <QString>
#include <QTextStream>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <vector>

typedef struct
{
     int count;
     float data[200];
} Carray;

class ObjectFile
{

public:
    ObjectFile(QFile &filename);

    // File Functions
    bool FileExists();
    void PrintFile();

    // Vertex Functions - assuming vertices have 3 points each
    void GetVertices();
    void PrintStoredVertices();

    // Vertex Normal Functions
    void GetVNormals();
    void PrintStoredVNormals();

    // Face Functions
    void GetFaces();
    void PrintStoredFaces();

    Carray vertices;
    Carray vnormals;
    Carray faces;

private:
    QFile *file;
};

#endif // OBJECTFILE_H
