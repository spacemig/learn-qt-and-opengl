#-------------------------------------------------
#
# Project created by QtCreator 2016-04-14T08:28:30
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = x96Project
TEMPLATE = app

SOURCES += main.cpp\
        openglwindow.cpp \
    trianglewindow.cpp \
    squarewindow.cpp \
    cubewindow.cpp \
    circlewindow.cpp \
    spherewindow.cpp \
    objectfile.cpp \
    objcube.cpp

HEADERS  += openglwindow.h \
    trianglewindow.h \
    squarewindow.h \
    cubewindow.h \
    circlewindow.h \
    spherewindow.h \
    objectfile.h \
    objcube.h

RESOURCES += \
    3dfiles.qrc \
    shaders.qrc
