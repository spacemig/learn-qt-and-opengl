#include "openglwindow.h"
#include "objectfile.h"
#include "objcube.h"

using namespace std;

ObjCube::ObjCube()
    : shaderprogram(0),
      frame(0)
{
}

void ObjCube::initialize()
{
    shaderprogram = new QOpenGLShaderProgram(this);
    shaderprogram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":Vshaders/squareshader.vsh");
    shaderprogram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":Vshaders/squareshader.fsh");
    shaderprogram->link();
    position = shaderprogram->attributeLocation("posAttr");
    color = shaderprogram->attributeLocation("colAttr");
    matrixUniform = shaderprogram->uniformLocation("matrix");

    QFile file(":wavefront_files/cube.obj");
    ObjectFile objfile(file);

    // get the vertices according to the faces
    for(int i = 0, a = 0, b = 1, c = 2, k = 0; i < objfile.faces.count; a+=3, b+=3, c+=3, i+=3)
    {
        k = (objfile.faces.data[i] - 1)*3;

        intvertices.data[a] = objfile.vertices.data[k];
        intvertices.data[b] = objfile.vertices.data[k+1];
        intvertices.data[c] = objfile.vertices.data[k+2];

        intvertices.count+=3;
    }
}

void ObjCube::render()
{
    const qreal retinaScale = devicePixelRatio();

    // This adjusts your screen to your desktop/laptop
    glViewport(0,0, width()*retinaScale, height()*retinaScale);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shaderprogram->bind();

    // This is the matrix that takes care of the views
    QMatrix4x4 matrix;
    matrix.perspective(50.0f, 4.0f/3.0f, 0.1f, 100.0f);
    matrix.translate(-0.25, -0.25, -2);
    matrix.rotate(50.0f * frame / screen()->refreshRate(), 0, 1, 0); // controls the rotation speed

    shaderprogram->setUniformValue(matrixUniform, matrix);

    GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 1.0f, 1.0f,   // front left triangle |\ |
        1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,   // front right triangle \|
        0.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 1.0f

    };

    glEnable(GL_CULL_FACE);

    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, intvertices.data);
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 0, colors);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    shaderprogram->release();

    ++frame;
}










