#ifndef OBJCUBE_H
#define OBJCUBE_H

#include "openglwindow.h"
#include "objectfile.h"
#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>

#include <QtCore/qmath.h>

class ObjCube : public OpenGLWindow
{

public:
    ObjCube();

    void initialize() Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

private:

    GLuint position;
    GLuint color;
    GLuint matrixUniform;

    Carray intvertices;

    QOpenGLShaderProgram *shaderprogram;
    int frame;
};

#endif // OBJCUBE_H
