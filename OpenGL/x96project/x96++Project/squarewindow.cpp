#include "squarewindow.h"

// Constructor
SquareWindow::SquareWindow()
    : m_program(0),
      m_frame(0)
{
}

void SquareWindow::initialize()
{
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":Vshaders/squareshader.vsh");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":Vshaders/squareshader.fsh");
    m_program->link();
    m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_matrixUniform = m_program->uniformLocation("matrix");
}


void SquareWindow::render()
{
    const qreal retinaScale = devicePixelRatio();

    // This adjusts your screen to your desktop/laptop
    glViewport(0,0, width()*retinaScale, height()*retinaScale);

    glClear(GL_COLOR_BUFFER_BIT);

    m_program->bind();

    // This is the matrix that takes care of the views
    QMatrix4x4 matrix;
    matrix.perspective(60.0f, 4.0f/3.0f, 0.1f, 100.0f); // verticalAngle, aspectRatio, nearPlane, farPlane
    matrix.translate(0, 0, -2); // translates coordinates by x, y, z
    matrix.rotate(100.0f * m_frame / screen()->refreshRate(), 0, 1, 0);  // controls the rotation speed, x, y, z axes

    m_program->setUniformValue(m_matrixUniform, matrix);

    // The vertices for a suqare
    GLfloat vertices[] = {
        -0.5f, 0.50f,
        0.5f, 0.5f,
        0.5f, -0.5f,
        -0.5f, -0.5f
    };

    GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f
    };

    glVertexAttribPointer(m_posAttr, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(m_colAttr, 3, GL_FLOAT, GL_FALSE, 0, colors);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_QUADS, 0, 4);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    m_program->release();

    ++m_frame;
}














