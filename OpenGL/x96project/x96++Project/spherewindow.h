#ifndef SPHEREWINDOW_H
#define SPHEREWINDOW_H

#include "openglwindow.h"
#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <math.h>

class SphereWindow : public OpenGLWindow
{

public:
    SphereWindow();

    void initialize() Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

private:
    GLuint loadShader(GLenum type, const char *source);

    GLuint sphere_position;
    GLuint sphere_color;
    GLuint sphere_uniMatrix;

    QOpenGLShaderProgram *sphere_shader;

    int sphere_frame;
};

#endif // SPHEREWINDOW_H
