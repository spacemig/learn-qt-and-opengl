#include "circlewindow.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace std;

CircleWindow::CircleWindow()
    : circle_shader(0),
      circle_frame(0)
{
}

/* SHADER PROGRAMS START HERE */

static const char *vertexShaderSource =
    "attribute highp vec4 posAttr;\n"
    "attribute lowp vec4 colAttr;\n"
    "varying lowp vec4 col;\n"
    "uniform highp mat4 matrix;\n"
    "void main() {\n"
    "   col = colAttr;\n"
    "   gl_Position = matrix * posAttr;\n"
    "}\n";

static const char *fragmentShaderSource =
    "varying lowp vec4 col;\n"
    "void main() {\n"
    "   gl_FragColor = col;\n"
    "}\n";

GLuint CircleWindow::loadShader(GLenum type, const char *source)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, 0);
    glCompileShader(shader);
    return shader;
}

void CircleWindow::initialize()
{
    circle_shader = new QOpenGLShaderProgram(this);
    circle_shader->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    circle_shader->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    circle_shader->link();
    circle_position = circle_shader->attributeLocation("posAttr");     // take this info from the shaders
    circle_color = circle_shader->attributeLocation("colAttr");        // " "
    circle_uniMatrix = circle_shader->uniformLocation("matrix");       // " "
}


void CircleWindow::render()
{
    const qreal retinaScale = devicePixelRatio();

    // This adjusts your screen to your desktop/laptop
    glViewport(0,0, width()*retinaScale, height()*retinaScale);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);      // clears the screen to default - black

    circle_shader->bind();                                   // bind the programs together

    // This is the matrix that takes care of the views
    QMatrix4x4 matrix;
    matrix.perspective(100.0f, 4.0f/3.0f, 0.1f, 100.0f);
    matrix.translate(0, 0, -2);
    matrix.rotate(50.0f * circle_frame / screen()->refreshRate(), 0, 0, 1); // controls the rotation speed; axis doesn't change speed much

    circle_shader->setUniformValue(circle_uniMatrix, matrix);

    const int sides = 40;  // The amount of segment to create the circle
    const double radius = 2; // The radius of the circle

    const int size = sides*2;

    GLfloat vertices[size];

    for(int a = 0, b = 0, c = 1; a < 360 && b < (size) && c < (size); a += 360/sides, b+=2, c+=2)
    {
        double heading = a * 3.1415926535897932384626433832795 / 180;
        vertices[b]   = cos(heading) * radius;
        vertices[c]   = sin(heading) * radius;
    }

    GLfloat colors[(size)];

    /*
    for(int a = 0, ca = 0, cb = 1; a < 360 && ca < size && cb < size; a+= 360/sides,ca+=2, cb+=2)
    {
        double heading = a * 3.1415926535897932384626433832795 / 180;
        colors[ca] = cos(heading);
        colors[cb] = sin(heading);
    }*/

    for(int a = 0, b = 1, c = 2; a < size && b < size && c < size; a+=3, b+=3, c+=3)
    {
        colors[a] = 0.8;
        colors[b] = 0.6;
        colors[c] = 0.5;
    }

    glVertexAttribPointer(circle_position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(circle_color, 2, GL_FLOAT, GL_FALSE, 0, colors);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_LINE_LOOP, 0, sides);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    circle_shader->release();

    ++circle_frame;
}













