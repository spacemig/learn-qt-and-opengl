#include "spherewindow.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
using namespace std;

SphereWindow::SphereWindow()
    : sphere_shader(0),
      sphere_frame(0)
{
}

/* SHADER PROGRAMS START HERE */

static const char *vertexShaderSource =
    "attribute highp vec4 posAttr;\n"
    "attribute lowp vec4 colAttr;\n"
    "varying lowp vec4 col;\n"
    "uniform highp mat4 matrix;\n"
    "void main() {\n"
    "   col = colAttr;\n"
    "   gl_Position = matrix * posAttr;\n"
    "}\n";

static const char *fragmentShaderSource =
    "varying lowp vec4 col;\n"
    "void main() {\n"
    "   gl_FragColor = col;\n"
    "}\n";

GLuint SphereWindow::loadShader(GLenum type, const char *source)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, 0);
    glCompileShader(shader);
    return shader;
}

void SphereWindow::initialize()
{
    sphere_shader = new QOpenGLShaderProgram(this);
    sphere_shader->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    sphere_shader->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    sphere_shader->link();
    sphere_position = sphere_shader->attributeLocation("posAttr");     // take this info from the shaders
    sphere_color = sphere_shader->attributeLocation("colAttr");        // " "
    sphere_uniMatrix = sphere_shader->uniformLocation("matrix");       // " "
}


void SphereWindow::render()
{
    const qreal retinaScale = devicePixelRatio();

    // This adjusts your screen to your desktop/laptop
    glViewport(0,0, width()*retinaScale, height()*retinaScale);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);      // clears the screen to default - black

    sphere_shader->bind();                                   // bind the programs together

    // This is the matrix that takes care of the views
    QMatrix4x4 matrix;
    matrix.perspective(100.0f, 4.0f/3.0f, 0.1f, 100.0f);
    matrix.translate(0, 0, -2);
    matrix.rotate(50.0f * sphere_frame / screen()->refreshRate(), 0, 1, 0); // controls the rotation speed; axis doesn't change speed much

    sphere_shader->setUniformValue(sphere_uniMatrix, matrix);

    const int sides = 40;  // The amount of segment to create the sphere
    const double radius = 1; // The radius of the sphere

    const int size = sides*2;

    GLfloat vertices[size];

    /*
    for(int a = 0, b = 0, c = 1, d = 2; (a < 360) && (b < size && c < size && d < size) ; a += 360/sides, b+=3, c+=3, d+=3)
    {
        double heading = a * 3.1415926535897932384626433832795 / 180;
        vertices[b] = radius * sin(heading) * cos(heading);
        vertices[c] = sin(heading) * sin(heading) * radius;
        vertices[d] = radius * cos(heading);
    }
    */

int count = 0;

    for(int phi = 0, theta = 0, b = 0, c = 1, d = 2; phi < 360 && theta < 360 && (b < size || c < size || d < size); phi+= 10, theta+=10, b+=3, c+=3, d+=3)
    {
        double deg = phi * 3.1415926535897932384626433832795 / 180;

        vertices[b] = radius * cos(deg) * sin(deg);
        vertices[c] = radius * sin(deg) * sin(deg);
        vertices[d] = radius * cos(deg) * sin(deg);

        cout << vertices[b] << " " << vertices[c] << " " << vertices[d] << endl;
        count++;
    }

    cout << " " << endl;
    cout << count << endl;

    GLfloat colors[size];

    /*
    for(int a = 0, ca = 0, cb = 1; a < 360 && ca < size && cb < size; a+= 360/sides,ca+=2, cb+=2)
    {
        double heading = a * 3.1415926535897932384626433832795 / 180;
        colors[ca] = cos(heading);
        colors[cb] = sin(heading);
    }
    */

    for(int a = 0, b = 1, c = 2; a < size || b < size || c < size; a+=3, b+=3, c+=3)
    {
        colors[a] = 0.8;
        colors[b] = 0.6;
        colors[c] = 0.5;
    }

    /*
    GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 1.0f, 1.0f,   // front left triangle |\ |
        1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,   // front right triangle \|
        0.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 1.0f

    };*/

    glVertexAttribPointer(sphere_position, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(sphere_color, 3, GL_FLOAT, GL_FALSE, 0, colors);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_TRIANGLES, 0, 27);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    sphere_shader->release();

    ++sphere_frame;
}








