#ifndef CIRCLEWINDOW_H
#define CIRCLEWINDOW_H

#include "openglwindow.h"
#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <math.h>

class CircleWindow : public OpenGLWindow
{

public:
    CircleWindow();

    void initialize() Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

private:
    GLuint loadShader(GLenum type, const char *source);

    GLuint circle_position;
    GLuint circle_color;
    GLuint circle_uniMatrix;

    QOpenGLShaderProgram *circle_shader;

    int circle_frame;
};

#endif // CIRCLEWINDOW_H
