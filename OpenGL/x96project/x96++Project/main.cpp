#include "openglwindow.h"
#include "trianglewindow.h"
#include "squarewindow.h"
#include "cubewindow.h"
#include "circlewindow.h"
#include "spherewindow.h"
#include "objectfile.h"
#include "objcube.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <QtCore/qmath.h>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

using namespace std;

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    // get 3D file ---> test file is stored in project folder
    //QFile file(":wavefront_files/cube.obj");
    //ObjectFile objfile(file);
    //objfile.PrintStoredVertices();
    //objfile.PrintStoredFaces();

    QSurfaceFormat format;
    format.setSamples(16);

    /*
    CubeWindow cbwindow;
    cbwindow.setFormat(format);
    cbwindow.resize(640, 480);
    cbwindow.show();

    cbwindow.setAnimating(true);

    ObjCube ocubewindow;
    ocubewindow.setFormat(format);
    ocubewindow.resize(640, 480);
    ocubewindow.show();

    ocubewindow.setAnimating(true);
    */

    SquareWindow swindow;

    swindow.setFormat(format);
    swindow.resize(640, 480);
    swindow.show();

    /*
    TriangleWindow twindow;
    twindow.setFormat(format);
    twindow.resize(640, 480);
    twindow.show();

    twindow.setAnimating(true);


    swindow.setAnimating(true);


    CircleWindow cwindow;
    cwindow.setFormat(format);
    cwindow.resize(640, 480);
    cwindow.show();

    cwindow.setAnimating(true);

    SphereWindow sphwindow;
    sphwindow.setFormat(format);
    sphwindow.resize(640, 480);
    sphwindow.show();

    sphwindow.setAnimating(true);
*/

    return app.exec();

}
