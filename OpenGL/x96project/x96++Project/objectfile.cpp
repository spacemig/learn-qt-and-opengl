#include "objectfile.h"
#include <stdio.h>
#include <stdlib.h>

#include <QFile>
#include <QString>
#include <QTextStream>
#include <iostream>

using namespace std;

// ------------------ File Functions -------------------- //

ObjectFile::ObjectFile(QFile &filename)
{
    file = &filename;
    vertices.count = 0;
    vnormals.count = 0;
    faces.count = 0;

    // add the "Get" vertices / faces / vertex normals / etc. to this
    GetVertices();
    GetVNormals();
    GetFaces();
}

bool ObjectFile::FileExists()
{
    if(file->exists())
    {
        cout << "exists" << endl;
        return 1;
    }

    else
    { return 0; }
}

void ObjectFile::PrintFile()
{
    // find a better way to control the opening and closing
    QTextStream text(file);

    file->open(QFile::ReadOnly | QFile::Text);

    while(!text.atEnd())
    {
        QString line = text.readLine();
        cout << line.toStdString() << endl;
    }

    file->close();
}

// ------------------ Vertex Functions -------------------- //

void ObjectFile::GetVertices()
{
    QTextStream text(file);

    // find a better way to control the opening and closing
    file->open(QFile::ReadOnly | QFile::Text);

    int a = 0;
    int b = 1;
    int c = 2;

    while(!text.atEnd())
    {
        QString line = text.readLine();

        // leave a space after v so it doesn't catch "vn"
        if(line.contains("v "))
        {
            QRegExp rx("[ ]"); // match a space
            QStringList list = line.split(rx, QString::SkipEmptyParts);

            vertices.data[a] = list.at(1).toFloat();
            vertices.data[b] = list.at(2).toFloat();
            vertices.data[c] = list.at(3).toFloat();

            a += 3;
            b += 3;
            c += 3;

            vertices.count += 3;
        }
    }

    file->close();
}

void ObjectFile::PrintStoredVertices()
{
    cout << "Vertices Count: " << vertices.count << endl;

    for(int j = 0; j < vertices.count; j += 3)
    {
        cout << vertices.data[j] << " " << vertices.data[j+1] << " " << vertices.data[j+2] << endl;
    }
}

// ------------------ Vertex Normal Functions -------------------- //

void ObjectFile::GetVNormals()
{
    QTextStream text(file);

    // find a better way to control the opening and closing
    file->open(QFile::ReadOnly | QFile::Text);

    int a = 0;
    int b = 1;
    int c = 2;

    while(!text.atEnd())
    {
        QString line = text.readLine();

        // leave a space after v so it doesn't catch "vn"
        if(line.contains("vn"))
        {
            QRegExp rx("[ ]"); // match a space
            QStringList list = line.split(rx, QString::SkipEmptyParts);

            vnormals.data[a] = list.at(1).toFloat();
            vnormals.data[b] = list.at(2).toFloat();
            vnormals.data[c] = list.at(3).toFloat();

            a += 3;
            b += 3;
            c += 3;

            vnormals.count += 3;
        }
    }

    file->close();
}

void ObjectFile::PrintStoredVNormals()
{
    cout << "VNormals Count: " << vnormals.count << endl;

    for(int j = 0; j < vnormals.count; j += 3)
    {
        cout << vnormals.data[j] << " " << vnormals.data[j+1] << " " << vnormals.data[j+2] << endl;
    }
}

// ------------------ Face Functions -------------------- //

void ObjectFile::GetFaces()
{
    QTextStream text(file);

    // find a better way to control the opening and closing
    file->open(QFile::ReadOnly | QFile::Text);

    int a = 0;
    int b = 1;
    int c = 2;

    while(!text.atEnd())
    {
        QString line = text.readLine();

        if(line.contains("f "))
        {
            QRegExp rx("\\b");
            QStringList list = line.split(rx);

            faces.data[a] = list.at(3).toFloat();
            faces.data[b] = list.at(4).toFloat();
            faces.data[c] = list.at(5).toFloat();

            a += 3;
            b += 3;
            c += 3;

            faces.data[a] = list.at(7).toFloat();
            faces.data[b] = list.at(8).toFloat();
            faces.data[c] = list.at(9).toFloat();

            a += 3;
            b += 3;
            c += 3;

            faces.data[a] = list.at(11).toFloat();
            faces.data[b] = list.at(12).toFloat();
            faces.data[c] = list.at(13).toFloat();

            a += 3;
            b += 3;
            c += 3;

            faces.count += 9;
        }
    }

    file->close();
}

void ObjectFile::PrintStoredFaces()
{
    cout << "Faces Count: " << faces.count << endl;
    cout << endl;
    cout << "v vt vn ---- vertex | vertex texture | vertex normal" << endl;

    for(int j = 0; j < faces.count; j += 9)
    {
        cout << faces.data[j]   << " " << faces.data[j+1] << " " << faces.data[j+2] << "  "
             << faces.data[j+3] << " " << faces.data[j+4] << " " << faces.data[j+5] << "  "
             << faces.data[j+6] << " " << faces.data[j+7] << " " << faces.data[j+8] <<endl;
    }
}















