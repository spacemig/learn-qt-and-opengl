#include "openglwindow.h"
#include "trianglewindow.h"
#include "squarewindow.h"
#include "cubewindow.h"


#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>

#include <QtCore/qmath.h>

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QSurfaceFormat format;
    format.setSamples(16);

    /*
    TriangleWindow window;
    window.setFormat(format);
    window.resize(640, 480);
    window.show();*/

    /*
    SquareWindow window;
    window.setFormat(format);
    window.resize(640, 480);
    window.show();
    */

    CubeWindow window;
    window.setFormat(format);
    window.resize(640, 480);
    window.show();

    window.setAnimating(false);

    return app.exec();
}
