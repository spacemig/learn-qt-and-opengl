#-------------------------------------------------
#
# Project created by QtCreator 2016-01-22T17:45:40
#
#-------------------------------------------------

QT       += core gui opengl widgets

TARGET = MyOpenGL
TEMPLATE = app


SOURCES += main.cpp\
        window.cpp \
    myglwidget.cpp

HEADERS  += window.h \
    myglwidget.h

LIBS += -lopengl32 -lglu32 -lglut32

FORMS    += window.ui
