# Learn Qt and OpenGL

Resources

* http://doc.qt.io/qt-5/qopenglwidget.html
* http://doc.qt.io/qt-5/examples-widgets-opengl.html
* http://doc.qt.io/qt-5/qtgui-index.html
* http://doc.qt.io/qt-5/qt3d-index.html
* http://doc.qt.io/qt-5/qt3d-examples.html --> look at C++ examples

specially this one

* http://doc.qt.io/qt-5/qt3drenderer-basicshapes-cpp-example.html

* http://www.opengl-tutorial.org/
* https://open.gl/
* http://learnopengl.com/


Equivalence between OpenGL functions and QtOpenGL widget functions

Gluint vbo 